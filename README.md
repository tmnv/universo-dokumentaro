# Общая информация

В этом репозитории создаётся документация Технокома, в том числе по Реальному виртуальному миру Универсо и Умной социальной сети Технокома.

В репозитории проект на Sphinx Documentation.

Так же [тут в Вики](https://gitlab.com/tehnokom/tehnokom-dokumentoj/-/wikis/home) собирается предварительная информация.


# Об этом репозитории

**Документация по Универсо**

[Этот проект на ReadTheDocs](https://tehnokom.readthedocs.io/)

ReadTheDocs сам компилирует проект при изменении.

Для полноценной работы над проектом понадобится его клонировать на свой компьютер и установить рабочие утилиты.

**Установка pip3**

Сначала убедимся, что Python 3 установлен в системе:

`python3 --version`

Команда выводит текущую версию Python, которая используется в системе. Теперь установим нужную версию PIP:

`sudo apt install python3-pip`

И смотрим информацию об установленной утилите:

`pip3 --version`

**Устанавливаем Sphinx Documentation**

`pip3 install -U Sphinx`

Устанавливаем тему ReadTheDocs

`pip3 install sphinx_rtd_theme`

Переходим в папку с клонированным проектом. Например

`cd universo-dokumentoj`

Переходим на рабочую ветку (например develop)

`git checkout develop`

Для того, чтоб скомпилировать проект, в рабочей папке выполняем команду

`make html`

Редактируемые файлы находятся в папке `source` (с расширением `.rst`).

Скомпилированные файлы проекта находятся в папке `build`.

[Документация по Sphinx Documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)



*Пролетарии всех стран, соединяйтесь!*
*Proletoj el ĉiuj landoj, unuiĝu!*