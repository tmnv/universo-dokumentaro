.. Universo Dokumentoj documentation master file, created by
   sphinx-quickstart on Tue Feb 25 18:00:43 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   Пролетарии всех стран, соединяйтесь!
   Proletoj el ĉiuj landoj, unuiĝu!

Добро пожаловать в Универсо!
===============================================

.. toctree::
   :maxdepth: 3
   :caption: Содержание:
   
   plan
   contrib


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
